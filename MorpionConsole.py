#!/usr/bin/python3

from Jeu import Jeu as Game
from Joueur import Joueur as Player
from Ordinateur import Ordinateur as Computer

class Main(object):

    @staticmethod
    def play(player, player2, game):
        game.grid_initialize()
        game.display_grid()
        while True:
            #Ask the player to choose a box
            case = player.getBox(game)
            game.set_grid(case, type=player.get_name() )

            game_over = game.game_over()

            #Computer plays
            case = Computer.getBox( game )
            if not case == None and game_over['winner'] == None :
                game.set_grid( case )

            game.display_grid()

            game_over = game.game_over()
            if not game_over["winner"] == None:
                print("\033[1;31m" + game_over["winner"] + " a gagne la partie !\033[0m")
                break

            if game.grid_is_full():
                print("\033[1;31mEgalite !\033[0m")
                break


if __name__ == '__main__':
    """    Create a new players    """
    player_name = input("veuillez entrer votre nom :\n>>> ")
    player = Player(name=player_name)
    player2 = Player(name="Ordinateur")

    """    initialize the game    """
    game = Game(player, player2)

    rejouer = "o"
    while rejouer == "o":
        Main.play(player, player2, game)
        rejouer = input("Voulez-vous rejouer ?(o/n):\n>>> ").lower()

    print("\033[1;35m******************* A bientôt Champion(ne) ! *******************\033[0m")
