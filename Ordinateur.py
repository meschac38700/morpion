from random import choice
from Utilities import Utilities
class Ordinateur(object):

    @staticmethod
    def getBox( game_instance ):

        print("\033[0;35mcomputer thinking...\033[0m")
        case = None
        if  game_instance.grid_is_full() :
            return None
        while not game_instance.is_empty_case( case ):
            row = choice(["1", "2", "3"])
            col = choice(["A", "B", "C"])
            case = row+col
        return case
