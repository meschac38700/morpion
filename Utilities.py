class Utilities():


    # Convert Letter (A,B,C) to array index (0,1,2)
    """
        :param p_case = the case, format string
        example => "1a" == (0,0) or "2a" == (1,0) etc ... that's the position of the case
        :return []
    """
    @staticmethod
    def convertLettreAIndice( p_case ):
        try:
            goodCase = []
            goodCase.append( int( p_case[0] ) - 1 )
            if p_case[1].isdigit():
                goodCase.append( int(p_case[1]) - 1 )
            elif p_case[1].lower() == "a":
                goodCase.append(0)
            elif p_case[1].lower() == "b":
                goodCase.append(1)
            elif p_case[1].lower() == "c":
                goodCase.append(2)
        except:
            print("\033[1;31mConvertLettreAIndice: Invalid case\033[0m")
            return None
        return goodCase

    @staticmethod
    def is_good_case( case, display_error_message = False ):
        if not case == None and not case == "" and isinstance(case, str):

            if case[0].isdigit() and (int(case[0]) <= 3  and int(case[0]) > 0):
                if case[1].isalpha() and case[1].upper() in "ABC":
                    return True
                elif case[1].isdigit() and (int(case[1]) <= 3  and int(case[1]) > 0):
                    return True
        print("\033[1;31mIs_good_case: Invalid case\033[0m") if display_error_message else print("")
        return False


    # Check if an array is full
    @staticmethod
    def isFull(game, tab):

        return  not game.CASE_VIDE in tab


    @staticmethod
    def contains_only_computer_char(game, tab ):
        return not game.JOUEUR in tab and not game.CASE_VIDE in tab

    @staticmethod
    def contains_only_player_char(game, tab):
        return not game.ORDI in tab and not game.CASE_VIDE in tab
