from Utilities import Utilities

class Jeu:
    ROW = 3
    COLUMN = 3
    CASE_VIDE = "-"
    JOUEUR = "X"
    ORDI = "O"
    def __init__(self, player_one, player_two= "Ordinateur"):

        self.player = player_one
        print("Le jeu se jouera avec les joueurs suivants : {} et {} ".format(player_one.get_name(),player_two.get_name()))
        self.grille =[]

    """
        Initialisation d'une grille vide
    """
    def grid_initialize(self ):
        self.grille = []
        for i in range(Jeu.ROW):
            liste = []
            for j in range(Jeu.COLUMN):
                liste.append(Jeu.CASE_VIDE)
            self.grille.append(liste)

    # display the grid
    def display_grid(self ):
        print("  A B C")
        count = 1
        for i in range(len(self.grille[0])):
            print(count, end=" ")
            for j in range(len(self.grille[1])):
                print(self.grille[i][j], end=" ")
            print()
            count += 1

    # Check if the position is empty
    """
        i, j = case position  i for line and j for the column
    """
    def is_empty_case(self, i, j):
        if not Utilities.is_good_case( [i,j] ):
            return False
        if self.grille[i][j] == Jeu.CASE_VIDE:
            return True
        return False

    # Check if the position is empty
    """
        case = [i,j]
        case that is the position of the case played...
    """
    def is_empty_case(self, case):

        if Utilities.is_good_case(case):
            case = Utilities.convertLettreAIndice( case )
            if self.grille[case[0]][case[1]] == Jeu.CASE_VIDE:
                return True
        return False

    # ajout X|O dans la grille selon la case choisit par le joueur
    def set_grid(self, case, type="Ordinateur"):

        #Conversion de la case jouer par le joueur
        case = Utilities.convertLettreAIndice(case)

        #Verification de l'identité du joueur
        if type == "Ordinateur":

                self.grille[case[0]][case[1]] = Jeu.ORDI

        else:

            self.grille[case[0]][case[1]] = Jeu.JOUEUR

        return self.grille


    # Check if grid is full
    def grid_is_full(self):

        for i in range(len(self.grille)):
            if Utilities.isFull(self, self.grille[i]):
                continue
            else:
                return False
        return True


    def _game_over_scenario_1(self):
        """
           Si les meme pions sont alignés en horizontal dans la grille
           gagner en verticale

            |O O O| |- - -| |- - -|
            |- - -| |O O O| |- - -|
            |- - -| |- - -| |O O O|

        """
        for i in range(len(self.grille)):

            if Utilities.isFull(self, self.grille[i]):

                if Utilities.contains_only_computer_char(self, self.grille[i]):
                    return {"winner": "Computer"}
                elif Utilities.contains_only_player_char(self, self.grille[i]):
                    return {"winner": self.player.get_name()}
        return {"winner": None}


    def _game_over_scenario_2(self):
        """
            Si les meme pions sont alignés en vertical dans la grille
            gagner en verticale
            |O - -| |- O -| |- - O|
            |O - -| |- O -| |- - O|
            |O - -| |- O -| |- - O|
        """
        for i in range(len(self.grille)):

            tmp_grille = []
            for j in range(len(self.grille[i])):
                tmp_grille.append(self.grille[j][i])
            if Utilities.isFull(self, tmp_grille):

                if Utilities.contains_only_computer_char(self, tmp_grille):
                    return {"winner": "Computer"}
                elif Utilities.contains_only_player_char(self, tmp_grille):
                    return {"winner": self.player.get_name()}
        return {"winner": None}


    def _game_over_scenario_3(self):
        """
               gagner en diagonale
               |O - -|
               |- O -|
               |- - O|
        """
        tmp_grille = []
        for i in range(len(self.grille)):
            tmp_grille.append(self.grille[i][i])
        if Utilities.isFull(self, tmp_grille):

            if Utilities.contains_only_computer_char(self, tmp_grille):
                return {"winner": "Computer"}
            elif Utilities.contains_only_player_char(self, tmp_grille):
                return {"winner": self.player.get_name()}
        return {"winner": None}

    def _game_over_scenario_4(self):
        """
        gagner en diagonale inversée
               |- - O|
               |- O -|
               |O - -|
        :param tab:
        :return:
        """
        tmp_grille = []
        for i in range(len(self.grille)):
            tmp_grille.append(self.grille[i][(i + 1) * -1])
        if Utilities.isFull(self, tmp_grille):

            if Utilities.contains_only_computer_char(self, tmp_grille):
                return {"winner": "Computer"}
            elif Utilities.contains_only_player_char(self, tmp_grille):
                return {"winner": self.player.get_name()}
        return {"winner": None}

    # check if someone won the game
    def game_over( self):

        # Cas 1 gagnant en ligne
        winner_1 = self._game_over_scenario_1()
        # cas 2 gagnant en colonne
        winner_2 = self._game_over_scenario_2()
        # Cas 3 : gagnant en diagonal
        winner_3 = self._game_over_scenario_3()
        # Cas 4 : gagnant en diagonal inverse
        winner_4 = self._game_over_scenario_4()

        if winner_1["winner"]:
            return winner_1

        elif winner_2["winner"]:
            return winner_2

        elif winner_3["winner"]:
            return winner_3

        else:
            return winner_4


    @staticmethod
    def ask_for_replay():
        rejouer = input("Voulez-vous rejouer ?(o/n):\n>>> ").lower()
        if rejouer == "o":
            return True
        else:
            print("A bientôt !")
            return False