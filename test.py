import random
import unittest

from Jeu import Jeu
from Ordinateur import Ordinateur
from Joueur import Joueur
from Utilities import Utilities 

class MorpionTest(unittest.TestCase):

	def setUp(self):
		joueur1 = Joueur("Beta Test 1")
		joueur2 = Joueur("Beta Test 2")
		self.ordi = Ordinateur()
		self.game= Jeu(joueur1, joueur2)
		self.game.grid_initialize()

	def test_empty_grid(self):
		grid_length = 3
		self.assertEqual(grid_length, len(self.game.grille))
		sub_grid_length = 3
		for i in range(3):
			self.assertNotIn(self.game.JOUEUR, self.game.grille[i])
			self.assertNotIn(self.game.ORDI, self.game.grille[i])
			self.assertIn(self.game.CASE_VIDE, self.game.grille[i])
			self.assertEqual(sub_grid_length, len(self.game.grille[i]) )
		
		
	def test_play_specific_box(self):
		case = self.ordi.getBox(self.game)
		self.game.set_grid(case)
		good_case = Utilities.convertLettreAIndice(case) 

		self.assertEqual(Jeu.ORDI, self.game.grille[ good_case[0] ][ good_case[1] ])


	def test_is_empty_box(self):
		#get a empty new box
		not_empty_box = self.ordi.getBox(self.game)
		#play this box
		self.game.set_grid(not_empty_box)
		#get a new empty box don't play it and check
		empty_box = self.ordi.getBox(self.game)

		self.assertEqual(False, self.game.is_empty_case(not_empty_box) )
		self.assertEqual(True, self.game.is_empty_case(empty_box) )



	def test_winner_horizontally(self):
		self.game.grille[0][0]	= "X"
		self.game.grille[0][1]	= "X"
		self.game.grille[0][2]	= "X"
		expected = {"winner": self.game.player.get_name()}
		self.assertEqual(expected, self.game._game_over_scenario_1())	


	def test_winner_in_vertically(self):
		self.game.grille[0][0]	= "X"
		self.game.grille[1][0]	= "X"
		self.game.grille[2][0]	= "X"
		expected = {"winner": self.game.player.get_name()}
		self.assertEqual(expected, self.game._game_over_scenario_2())


	def test_winner_in_left_diagonally(self):
		self.game.grille[0][0]	= "X"
		self.game.grille[1][1]	= "X"
		self.game.grille[2][2]	= "X"
		expected = {"winner": self.game.player.get_name()}
		self.assertEqual(expected, self.game._game_over_scenario_3())

	def test_winner_in_right_diagonally(self):
		self.game.grille[0][2]	= "X"
		self.game.grille[1][1]	= "X"
		self.game.grille[2][0]	= "X"
		expected = {"winner": self.game.player.get_name()}
		self.assertEqual(expected, self.game._game_over_scenario_4())

	def test_convert_lettre_to_indice(self):
		case = "1C"
		expected = [0,2]
		actual = Utilities.convertLettreAIndice(case)

		self.assertEqual(expected, actual)


	def test_contains_only_computer_char(self):
		good_grid = ["O","O","O","O","O"]
		bad_grid = ["O","O","X","O","O"]

		self.assertEqual(True, Utilities.contains_only_computer_char(self.game, good_grid))
		self.assertEqual(False, Utilities.contains_only_computer_char(self.game, bad_grid))


	def test_contains_only_player_char(self):
		good_grid = ["X","X","X","X","X"]
		bad_grid = ["X","X","X","O","O"]

		self.assertEqual(True, Utilities.contains_only_player_char(self.game, good_grid))
		self.assertEqual(False, Utilities.contains_only_player_char(self.game, bad_grid))

	def test_full_grid(self):
		not_full_grid = ['O','-', 'X']
		full_grid = ['O','O', 'X']
		self.assertEqual(True, Utilities.isFull(self.game, full_grid))
		self.assertEqual(False, Utilities.isFull(self.game, not_full_grid))


	def test_is_good_box(self):

		good_box = ["1A", "1B", "1C", "2A", "2B", "2C", "3A", "3B", "3C", "11", "12", "13", "21", "22", "23", "31", "32", "33"]
		bad_box  = ["AA", "AB", "AC", "BA", "BB", "BC", "CA", "CB", "CC"]
		for box in good_box:
			self.assertEqual(True, Utilities.is_good_case( box ) )

		for box in bad_box:
			self.assertEqual(False, Utilities.is_good_case( box ) )



unittest.main()