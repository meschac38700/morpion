#!/usr/bin/python3

from tkinter import *
from Jeu import Jeu as Game
from Joueur import Joueur as Player

class MorpionGraphique():

    COMPTEUR = 0
    PION_JOUEUR_1_TEXT = "{} : X"
    PION_JOUEUR_2_TEXT = "{} : O"
    TITLE = "{} VS {}"

    def __init__(self, p_player_one, p_player_two):
        
        self.player_one = Player(name=p_player_one)
        self.player_two = Player(name=p_player_two)

        self.game = Game(self.player_one, player_two=self.player_two)
        self.root = Tk()
        self.root.title("Welcome to morpion game")
        self.root.maxsize(650, 350)
        self.root.minsize(650, 350)

        self.mainframe = PanedWindow(self.root, orient= VERTICAL, name="mainframe", bg="#b2bec3")
        

        self.topbar = PanedWindow(self.mainframe, name="topbar", orient=VERTICAL, bg="#b2bec3")

        self.topbar.add(Label(self.topbar, text=self.TITLE.format(self.player_one.get_name(), self.player_two.get_name()), font="Helvetica 20 bold", fg="#d63031", bg="#b2bec3"))
        self.result = Label(self.topbar,font="Helvetica 10 bold", fg="#d35400", bg="#b2bec3" )
        self.topbar.add(self.result)
        self.topbar.pack(fill=X)
        
        #.pack(pady=(0, 20))

        self.body = PanedWindow(self.mainframe, name="middle", orient=HORIZONTAL, bg="#b2bec3")

        self.leftSide = PanedWindow(self.body, orient=VERTICAL, name="leftSide", bg="#b2bec3")
        
        self.body.add(self.leftSide)
        self.body.pack(fill=X)
        #self.leftSide.pack()

        self.leftSide.add(Label(self.leftSide, text=self.PION_JOUEUR_1_TEXT.format(self.player_one.get_name()), anchor=W, justify=LEFT,  bg="#b2bec3"))
        self.leftSide.add(Label(self.leftSide, text=self.PION_JOUEUR_2_TEXT.format(self.player_two.get_name()), anchor=W, justify=LEFT,  bg="#b2bec3"))

        self.rightSide = Frame(self.body, bg="#b2bec3")
        self.body.add(self.rightSide)
        self.body.paneconfig(self.rightSide, padx=(100), pady=(25))
        self.body.paneconfig(self.leftSide, padx=(50))

        #self.rightSide.pack(expand=Y, fill="both", pady=(20,0))

        self.initializeGrid()

        self.footer = PanedWindow(self.mainframe, name="footer", orient=HORIZONTAL, bg="#b2bec3")
        self.btn = Button(self.footer, text="Rejouer",  width=20,fg="#0984e3", bd=2, font="Helvetica 10 bold", command=self.initializeGrid )
        self.footer.add(self.btn)
        self.footer.paneconfig(self.btn, pady=(30), padx=(150))
        self.footer.pack()
        #btn.pack(pady=(10,0))

        self.mainframe.add(self.topbar)
        self.mainframe.add(self.body)
        self.mainframe.add(self.footer)

        self.mainframe.pack(expand=Y, fill="both")

    def load_window(self):
        self.root.mainloop()

    def initializeGrid(self):

        for ligne in range(3):
            for colonne in range(3):
                btn = Button(self.rightSide, text='-', borderwidth=1, width=5, height=2, state=NORMAL, name=str(ligne+1) + str(colonne+1))
                btn.bind("<Button-1>", lambda event : self.onclick(event))
                btn.grid(row=ligne, column=colonne)
        self.result.config(text="")
        self.game.grid_initialize()

    def disableMorpion(self):
        for child in self.rightSide.winfo_children():
            child['state'] = DISABLED

    def changeCursorForm(self, type):
        for child in self.rightSide.winfo_children():
            child.config(cursor="circle") if type == self.game.ORDI else child.config(cursor="cross")

    def onclick(self, event):
        widget_clicked = event.widget
        
        if widget_clicked.cget('text') == self.game.CASE_VIDE:
            if self.COMPTEUR % 2 == 0 and ( widget_clicked["state"] == ACTIVE or widget_clicked["state"] == NORMAL) :
                self.changeCursorForm(self.game.ORDI)
                widget_clicked['text'] = self.game.JOUEUR

                case = widget_clicked._name

                self.game.set_grid(case, type=self.player_one.get_name())

                self.COMPTEUR +=1

            else:
                if widget_clicked['state'] == ACTIVE or widget_clicked['state'] == NORMAL:
                    self.changeCursorForm(self.game.JOUEUR)
                    widget_clicked['text'] = self.game.ORDI
                    """ Mode ORDINATEUR """
                    #case = Computer.getBox(self.game)
                    #if case :
                        #self.game.set_grid(case)
                        #self.COMPTEUR +=1
                    case = widget_clicked._name
                    self.game.set_grid(case)
                    self.COMPTEUR += 1
                    self.game.display_grid()

            """ Check if game over """
            game_over = self.game.game_over()
            vrai = game_over["winner"] != self.player_one.get_name() and game_over["winner"] != None
            game_over["winner"] = self.player_two.get_name() if vrai else game_over["winner"]
            if not game_over["winner"] == None:
                print("\033[1;31m" + game_over["winner"] + " a gagne la partie !\033[0m")
                self.result.config(text=game_over["winner"] + " a gagne la partie !")
                self.disableMorpion()

            if self.game.grid_is_full():
                print("\033[1;31mEgalite !\033[0m")
                self.result.config(text="Egalite")
                self.disableMorpion()



class Connexion():

    def __init__(self):

        self.root = Tk()

        self.root.title("Connexion")
        self.root.maxsize(400,200)
        self.root.minsize(350,200)
        mainFrame = Frame(self.root, padx=10, pady=20, bg="#b2bec3", width=600, height=300 )
        mainFrame.pack( expand=Y,fill="both")

        Label(mainFrame, text="Connexion", bg="#b2bec3", fg="#0984e3", font="Helvetica 30 bold").grid(row=0, column=1)

        Frame(mainFrame, height=20, bg="#b2bec3").grid(row=1)

        Label(mainFrame, text="Nom joueur 1 : ", fg="#0984e3", bg="#b2bec3", font="Helvetica 12 bold").grid(row=2, column=0, sticky=(E))
        Label(mainFrame, text="Nom joueur 2 : ", fg="#0984e3", bg="#b2bec3", font="Helvetica 12 bold").grid(row=3, column=0, sticky=(E))


        self.player_one = StringVar()
        self.player_two = StringVar()

        Entry(mainFrame, width=30, justify="center", textvariable=self.player_one).grid(row=2, column=1, sticky=(W))
        Entry(mainFrame, width=30, justify="center", textvariable=self.player_two).grid(row=3, column=1, sticky=(W))


        Button(mainFrame, text="Jouer", width=20,fg="#0984e3", bd=2, font="Helvetica 10 bold", command=self.get_player_name).grid(pady=(10,0),row=4, column=1)


    def load(self):
        self.root.mainloop()

    def get_player_name(self):
        if not self.player_one.get() == "" and not self.player_two.get() == "":

            self.root.destroy()

            morpion = MorpionGraphique(p_player_one=self.player_one.get(), p_player_two= self.player_two.get())

            morpion.load_window()

if __name__ == '__main__':
    Connexion().load()

