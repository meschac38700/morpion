from random import choice
from Utilities import Utilities
class Joueur(object):

    def __init__(self, name):
        self._name = name


    def __str__(self):
        return self._name

    def getBox(self, game_instance):
        """
            :param game_instance l'objet de la classe Jeu
            Demandez à l'utilisateur de rentrer une case :

            Tant qu'il ne rentre rien
            ou
            Tant qu'il rentre une case deja occupée :

        """
        case = None
        while case == "" or case == None or len(case) < 2:
            case = input("Choisir une case [nL] :\n>>> ")

        if not game_instance.is_empty_case(case) :
            return self.getBox( game_instance )
        return case

    def get_name(self):
        return self._name